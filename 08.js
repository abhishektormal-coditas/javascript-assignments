//typeof operator

//data types
//string "Abhishek"
//number 2
//booleans
//undefined
//null
//BigInt
//Symbol

let age = 22;
let firstName = "Abhishek";
console.log(typeof age);
console.log(typeof firstName);

//convert number to string
//age = String(age);
console.log(typeof(age+""))

//convert number to string
//age = Number(age);
let myStr = +"34";
console.log(typeof myStr);