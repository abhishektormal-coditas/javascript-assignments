//for of loop in array

const fruits = ["apple", "mango", "banana"];

for(let fruit of fruits){
    console.log(fruit);
}