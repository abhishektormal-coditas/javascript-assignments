//array push pop
//array shift unshift

let fruits = ["apple", "mango", "grapes"];
console.log(fruits);

//push
fruits.push("banana");
console.log(fruits);

//pop
fruits.pop();
console.log(fruits);

//unshift
fruits.unshift("banana");
fruits.unshift("cherry");
console.log(fruits);

//shift
let removedFruit = fruits.shift();
console.log("removed fruits is",removedFruit);
console.log(fruits);