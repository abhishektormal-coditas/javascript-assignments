//break keyword
for(let i=1; i<=10; i++){
    if(i===4){
        break;           
    }
    console.log(i);
}
console.log("for loop breaked");

console.log("Skipping 4 with continue keyword")
//continue -> skipping 4
for(let i=1; i<=10; i++){
    if(i===4){
        continue;         
    }
    console.log(i);
}