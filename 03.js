//rules for naming variables

//should not start with number
//var 1value = 10; //invalid
var value = 10; //valid
console.log(value);

//can use only underscore _ or dollar $
var first_name = "Abhishek"; //valid
console.log(first_name);
var _firstname = "Abhi"; //valid
console.log(_firstname);

var last$name; //valid
var $lastname; //valid

//cannot use spaces
//var first name; //invalid

//start with small letter & use camelCase
var firstName = "Abhishek";
console.log(firstName);
