// objects reference type  
// arrays are good but not sufficient 
// for real world data 
// objects store key value pairs 
// objects don't have index

//create objects 


const person = {
    name: "Abhishek",
    age: 22,
    hobbies: ["trekking", "riding bike", "listening music"]
}
console.log(person);

//access data from objects 
console.log(person["name"]);
console.log(person["age"]);
console.log(person.hobbies);

//add key value pair to objects
person.gender="male";
//person["gender"] = "male";
console.log(person);