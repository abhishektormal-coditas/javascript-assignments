// Maps
// map is an iterable

// store data in ordered fashion

// store key value pair (like object)
// duplicate keys are not allowed like objects


// difference between maps and objects

// objects can only have string or symbol
// as key 

// in maps you can use anything as key
// like array, number, string 

//key value pair 
const person = new Map();
person.set('firstName', 'Abhishek');
person.set('age', 22);
person.set(1,'one');
person.set([1,2,3],'onetwothree');
person.set({1: 'one'},'onetwothree');
console.log(person);
console.log(person.get(1));
// for(let key of person.keys()){
//     console.log(key, typeof key);
// }
for(let [key, value] of person){
    console.log(key, value)
}

const person1 = {
    id: 1,
    firstName: "Abhishek"
}
const person2 = {
    id: 2,
    firstName: "Aditya"
}

const extraInfo = new Map();
extraInfo.set(person1, {age: 22, gender: "male"});
extraInfo.set(person2, {age: 20, gender: "female"});

console.log(person1.id);
console.log(extraInfo.get(person1).gender);
console.log(extraInfo.get(person2).gender);