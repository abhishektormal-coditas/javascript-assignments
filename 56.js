//important array methods

//forEach

const numbers = [4,2,5,8];

function multiplyBy2(number){
    console.log(`${number}*2 = ${number*2}`);
}

// for(let i=0; i<numbers.length; i++){
//     multiplyBy2(numbers[i]);
// }

numbers.forEach(multiplyBy2);
numbers.forEach(function(number, index){
    console.log(number*3, index);
})