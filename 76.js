// const user1 = {
//     firstName : "Abhishek",
//     age: 18,
//     about: function(){
//         console.log(this.firstName, this.age);
//     }   
// }

//short syntax
const user1 = {
    firstName : "Abhishek",
    age: 18,
    about(){
        console.log(this.firstName, this.age);
    }   
}



user1.about();