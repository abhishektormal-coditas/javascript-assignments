//objects inside array
//useful for real world application

const users = [
    {user_id : 1, firstName : 'Abhishek', gender : 'male'},
    {user_id : 2, firstName : 'Aniket', gender : 'male'},
    {user_id : 3, firstName : 'Aditya', gender : 'male'}
]

for(let user of users){
    console.log(user);
}