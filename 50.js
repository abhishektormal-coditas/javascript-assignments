//block scope vs function scope

//let and cont are block scope
//var is function scope

{
    let firstName = "Abhishek";
    console.log(firstName);
}

//console.log(firstName); //ERROR

{
    const firstName = "Abhi";
    console.log(firstName);
}

//console.log(firstName); //ERROR

{
    var firstName = "ABHISHEK";
    console.log(firstName);
}

console.log(firstName); //valid

function myApp(){
    if(true){
        var firstName = "Abhishek"; //if we use let or const then it is not accessible outside if block
        console.log(firstName);
    }

    if(true){
        console.log(firstName);
    }
    console.log(firstName);
}

myApp();