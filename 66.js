// iterables 
// for loop applicable
// string , array are iterable 

const firstName = "Abhishek";
for(let char of firstName){
    console.log(char);
}

const items = ['tv', 'mobile', 'washing machine'];
for(let item of items){
    console.log(item);
}

// array like object 
// have length property
// acessible with index
// example :- string 

console.log(firstName.length);
console.log(firstName[2]);