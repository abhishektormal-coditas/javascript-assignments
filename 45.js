//function expression

const firstChar = function(anyString){
    return anyString[0];
}

const findTarget = function(array, target){
    for(let i=0; i<array.length; i++){
        if(array[i] === target){
            return i;
        }
    }
    return -1;
}

console.log(firstChar("Abhishek"));
const array = [1,2,3,4,5];
console.log(findTarget(array, 9));