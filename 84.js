//has own property

function CreateUser(firstName, lastName, email, age, address){
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.age = age;
    this.address = address;
}
CreateUser.prototype.about = function(){
    return `${this.firstName} is ${this.age} years old.`;
};
CreateUser.prototype.is18 = function (){
    return this.age >= 18; 
}
CreateUser.prototype.sing = function (){
    return "la la la la ";
}


const user1 = new CreateUser('Abhishek', 'Tormal', 'abhishek@gmail.com', 19, "Pune");
const user2 = new CreateUser('Aditya', 'Sonawane', 'aditya@gmail.com', 29, "Ahmednagar");
const user3 = new CreateUser('Kshitij', 'Bhagwat', 'kshitij@gmail.com', 17, "Akole");

for(let key in user1){
    if(user1.hasOwnProperty(key)){
        console.log(key);
    }
}