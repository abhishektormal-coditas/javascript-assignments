// and or operator

let firstName = "Abhishek";
let age = 22;

if(firstName[0] === "H" && age>18){
    console.log("inside if");
}else{
    console.log("inside else");
}

if(firstName[0] === "H" || age>18){
    console.log("inside if");
}else{
    console.log("inside else");
}