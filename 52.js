// rest parameters

function myFunc(number1, number2, ...restParameters){
    console.log(`number1 is ${number1}`);
    console.log(`number2 is ${number2}`);
    console.log(`restParameters is ${restParameters}`);
}

function addAll(...allParameters){
    let total = 0;
    for(let num of allParameters){
        total+=num;
    }
    return total;
}

myFunc(3, 4, 5, 6, 7, 8);
const ans = addAll(1,2,3,4,5,5,6,76,12);
console.log(ans);