//string indexing

let firstName = "Abhishek";

// A b h i s h e k
// 0 1 2 3 4 5 6 7

console.log(firstName[3]);

//length of string
console.log(firstName.length);

//last index
console.log(firstName.length-1);
