// function returning function

// function greet(){
//     function hello(){
//         return "hello world!!";
//     }
//     return hello;
// }

function greet(){
    return function(){
        return "Hello World!";
    }
}

const ans = greet();
console.log(ans());