// map method

// const numbers = [3,4,6,1,8];

// const square = function(number){
//     return number * number;
// }

// const squareNumber = numbers.map(number => number * number);
// console.log(squareNumber);

const users = [
    {firstName: "Abhi", age: 23},
    {firstName: "Adi", age: 21},
    {firstName: "Atharv", age: 22},
    {firstName: "Anand", age: 20},
]

const userNames = users.map((user)=>{
    return user.firstName;
});

console.log(userNames);