//arrays
//referece type
//order collection of items

let fruits = ["apple", "mango", "grapes"];
let mixed = [1,2,3,"string",null,undefined];

console.log(fruits);
console.log(mixed);


//array indexing

console.log(fruits[1]);
fruits[1] = "banana";
console.log(fruits);