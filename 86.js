// 2015 / es6
// class keyword
// class fake


class CreateUser{
    constructor(firstName, lastName, email, age, address){
        console.log("Constructor called");
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.age = age;
        this.address = address;
    }

    about(){
        return `${this.firstName} is ${this.age} years old.`;
    }
    is18(){
        return this.age >= 18;
    }
    sing(){
        return "la la la la ";
    }

}


const user1 = new CreateUser('Abhishek', 'Tormal', 'abhishek@gmail.com', 19, "Pune");
const user2 = new CreateUser('Aditya', 'Sonawane', 'aditya@gmail.com', 29, "Ahmednagar");
const user3 = new CreateUser('Kshitij', 'Bhagwat', 'kshitij@gmail.com', 17, "Akole");

console.log(user1.is18());
