// sort method

// 300,1200,45,13,20
// 13,20,45,300,1200 (expected)

const numbers = [300,1200,45,13,20];
numbers.sort();
console.log(numbers); //1200, 13, 20, 300, 45 (Wrong)

numbers.sort((a, b) => a-b); //for descending order b-a
console.log(numbers); //right result

//a-b ---> positive then swap
//a-b ---> negative then no swap

const userNames = ['abhishek','shubham','kshitij','aditya'];
userNames.sort();
console.log(userNames); //right result

const products = [
    {productId: 1, produceName: "headphones",price: 300 },
    {productId: 2, produceName: "adapter",price: 3000 },
    {productId: 3, produceName: "sticker",price: 200 },
    {productId: 4, produceName: "mobile",price: 8000 },
    {productId: 5, produceName: "t-shirt",price: 500 },
]

// lowToHigh
const lowToHigh = products.slice(0).sort((a,b)=>{
    return a.price-b.price
});

const highToLow = products.slice(0).sort((a,b)=>{
    return b.price-a.price;
});
