//object destructuring

const person = {
    firstName : "Abhishek",
    lastName : "Tormal",
    age : 22,
    address : "pune"
};

const {firstName, lastName, ...restDetails} = person;
console.log(firstName); 
console.log(restDetails);