// Sets (it is iterable)
// store data  
// sets also have its own methods
// No index-based access 
// Order is not guaranteed
// unique items only (no duplicates allowed)

const numbers = new Set([1,2,3]);
numbers.add(4);
numbers.add([5,6]); //possible to store arrays of same elemets
numbers.add([5,6]);
console.log(numbers);

if(numbers.has(1)){
    console.log("1 is present")
}else{
    console.log("1 is not present")
}

// for(let number of numbers){
//     console.log(number);
// }

const myArray = [1,2,4,4,5,6,5,6];
const uniqueElements = new Set(myArray);
console.log(uniqueElements);
console.log(myArray);