//what is prototype

function hello(){
    console.log("hello world");
}

// javascript function ===> function  + object

// console.log(hello.name);

// you can add your own properties 
hello.myOwnProperty = "very unique value";
console.log(hello.myOwnProperty);

// name property ---> tells function name;

// function provides more usefull properties.


console.log(hello.prototype); // {}

// only functions provide prototype property

hello.prototype.name = "Abhishek";
hello.prototype.age = 22;
hello.prototype.address = function(){
    return "pune";
};
console.log(hello.prototype.address());