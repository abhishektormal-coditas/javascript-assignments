//Use prototype

function createUser(firstName, lastName, email, age, address){
    const user =Object.create(createUser.prototype); //{} relationship established
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    return user;
}

createUser.prototype.about = function(){
    return `${this.firstName} is ${this.age} years old.`;
}

createUser.prototype.is18 = function(){
    return this.age >= 18;
}

createUser.prototype.sing = function(){
    return 'toon na na na la la ';
}

const user1 = createUser('Abhishek', 'Tormal', 'abhishek@gmail.com', 19, "Pune");
const user2 = createUser('Aditya', 'Sonawane', 'aditya@gmail.com', 29, "Ahmednagar");
const user3 = createUser('Kshitij', 'Bhagwat', 'kshitij@gmail.com', 17, "Akole");
console.log(user1.about());
console.log(user3.about());
console.log(user3.sing());