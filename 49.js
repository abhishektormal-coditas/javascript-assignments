//lexical scope

const myVar = "value1"
function myApp(){
    const myVar = "value2";
    function myFunc(){
        const myVar = "value3";
        console.log("inside myFunc", myVar);
    }
    // const myFunc2 = function(){}
    // const myFunc3 = () => {}

    console.log("inside myApp", myVar);
    myFunc();
}

console.log(myVar);
myApp();