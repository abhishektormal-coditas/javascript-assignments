//how to iterate object

const person = {
    name: "Abhishek",
    age: 22,
    hobbies: ["trekking", "riding bike", "listening music"]
};

//for in loop
for(let key in person){
    console.log(`${key} : ${person[key]}`);
}

//Object.keys
console.log(Object.keys(person));

for(let key of Object.keys(person)){
    console.log(person[key]);
}