// find method 

const myArray = ["Hello", "catt", "dog", "lion"];

const ans = myArray.find((string)=>string.length===3);
console.log(ans);

const users = [
    {userId : 1, userName: "abhishek"},
    {userId : 2, userName: "shubham"},
    {userId : 3, userName: "kshitij"},
    {userId : 4, userName: "aditya"},
];

const myUser = users.find((user)=>user.userId===3);
console.log(myUser);