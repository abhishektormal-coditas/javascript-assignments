//methods of string

let firstName = "  Abhishek ";
console.log(firstName.length);

//trim() for removing spaces
firstName = firstName.trim();
console.log(firstName);
console.log(firstName.length);

//toUpperCase()
console.log(firstName.toUpperCase());

//toLowerCase()
console.log(firstName.toLowerCase());

//slice()
console.log(firstName.slice(0,4));