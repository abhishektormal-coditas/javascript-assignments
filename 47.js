//hoisting

// hello();

// function hello(){
//     console.log("Hello World !");
// }

//if is not work with function expression and arrow functions

// hello();

// const hello = function(){
//     console.log("Hello World !");
// }

// hello();
// const hello = () => {
//     console.log("Hello World !");
// }

console.log(hello); //undefined 
var hello = "Hello !!"; //not work incase of let and const
console.log(hello);