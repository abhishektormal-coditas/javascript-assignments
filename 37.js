//difference between dot and bracket notation

const key = "email";
const person = {
    name: "Abhishek",
    age: 22,
    "person hobbies": ["trekking", "riding bike", "listening music"]
}

console.log(person["person hobbies"]);
//console.log(person.person hobbies); //ERROR

//person.key = "abhishek.tormal@coditas.com"; //key name is not email 
person[key] = "abhishek.tormal@coditas.com";
console.log(person);