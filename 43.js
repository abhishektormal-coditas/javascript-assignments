//nested destructuring

const users = [
    {user_id : 1, firstName : 'Abhishek', gender : 'male'},
    {user_id : 2, firstName : 'Aniket', gender : 'male'},
    {user_id : 3, firstName : 'Aditya', gender : 'male'}
]

const [{firstName: user1name}, , {gender}] = users;
console.log(user1name);
console.log(gender); 