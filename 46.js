//arrow functions

const greet = () => {
    console.log("Hey there !!");
}

const sumThreeNumbers = (number1, number2, number3) => {
    return number1 + number2 + number3;
}

const isEven = number => number % 2===0;

const multiplyTwoNumbers = (number1, number2) => number1 * number2;

greet();
const ans = sumThreeNumbers(2,3,4);
console.log(ans);
console.log(isEven(4));
console.log(multiplyTwoNumbers(19,2));