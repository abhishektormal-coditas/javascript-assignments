
function about(hobby, favMusician){
    console.log(this.firstName, this.age, hobby, favMusician);
}
const user1 = {
    firstName : "Abhishek",
    age: 18,   
}
const user2 = {
    firstName : "Asit",
    age: 19,
    
}

//about.call(user1,"guitar","Atif");

// apply
about.apply(user1, ["guitar", "Arijit"]);
const func = about.bind(user2, "guitar", "Sonu");
func();